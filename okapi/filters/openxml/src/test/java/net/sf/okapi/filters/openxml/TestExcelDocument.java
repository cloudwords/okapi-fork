package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.util.ArrayList;
import java.util.List;
import java.util.zip.ZipException;
import java.util.zip.ZipFile;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.JUnit4;

import net.sf.okapi.common.FileLocation;

import static org.junit.Assert.*;

/**
 * Misc tests related to XLSX processing.
 */
@RunWith(JUnit4.class)
public class TestExcelDocument {
	private XMLFactoriesForTest factories = new XMLFactoriesForTest();
	private FileLocation root;

	@Before
	public void setUp() {
		root = FileLocation.fromClass(getClass());
	}

	@Test
	public void testGetWorksheets() throws Exception {
		// Use a file with multiple sheets that appear out-of-order in the zip
		ExcelDocument doc = getExcelDocument("/ordering.xlsx", new ConditionalParameters());
		List<String> worksheets = doc.findWorksheets();
		List<String> expected = new ArrayList<String>();
		expected.add("xl/worksheets/sheet1.xml");
		expected.add("xl/worksheets/sheet2.xml");
		expected.add("xl/worksheets/sheet3.xml");
		doc.getZipFile().close();
		assertEquals(expected, worksheets);
	}

	@Test
	public void testGetSharedStrings() throws Exception {
		final ExcelDocument doc = getExcelDocument("/ordering.xlsx", new ConditionalParameters());
		try {
			final List<String> sharedStings = doc.findSharedStrings();
			assertEquals(1, sharedStings.size());
			assertEquals("xl/sharedStrings.xml", sharedStings.get(0));
		} finally {
			doc.getZipFile().close();
		}
	}

	@Test
	public void acceptsAbsentSharedStrings() throws Exception {
		final ExcelDocument doc = getExcelDocument("/850.xlsx", new ConditionalParameters());
		try {
			assertEquals(0, doc.findSharedStrings().size());
		} finally {
			doc.getZipFile().close();
		}
	}

	private ZipFile getZipFile(String resource) throws ZipException, IOException {
		return new ZipFile(root.in(resource).asFile());
	}

	private ExcelDocument getExcelDocument(String resource, ConditionalParameters params) throws Exception {
		OpenXMLZipFile ooxml = new OpenXMLZipFile(
			getZipFile(resource),
			factories.getInputFactory(),
			factories.getOutputFactory(),
			factories.getEventFactory(),
			StandardCharsets.UTF_8.name(),
			null,
			null,
			null
		);
		ExcelDocument doc = (ExcelDocument)ooxml.createDocument(params);
		doc.initialize();
		return doc;
	}
}
