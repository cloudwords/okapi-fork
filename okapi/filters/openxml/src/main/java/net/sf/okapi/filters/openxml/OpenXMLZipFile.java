/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.util.AbstractMap;
import java.util.Enumeration;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

import javax.xml.stream.XMLEventFactory;
import javax.xml.stream.XMLInputFactory;
import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;

import com.ctc.wstx.api.WstxInputProperties;

import net.sf.okapi.common.Util;
import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Excel;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Powerpoint;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Word;

/**
 * Wrapper around a regular ZipFile to provide additional
 * functionality.
 */
class OpenXMLZipFile {
	private static final String UNSUPPORTED_MAIN_DOCUMENT_TARGET = "Unsupported main document target";

	static final String CONTENT_TYPES_PART = "[Content_Types].xml";
	static final String ROOT_RELS_PART = "_rels/.rels";

	private static final String OFFICE_DOCUMENT = "/officeDocument";
	private static final String DOCUMENT = "/document";

	// The largest attribute I've ever seen in the wild is an o:gfxdata attribute that was just under
	// 1024*1024 characters.  We will double this to be safe.
	private static final int MAX_ATTRIBUTE_SIZE = 2 * 1024 * 1024;

	private final ZipFile zipFile;
	private final XMLInputFactory inputFactory;
	private final XMLOutputFactory outputFactory;
	private final XMLEventFactory eventFactory;
	private final String encoding;
	private final EncoderManager encoderManager;
	private final IFilter subfilter;
	private final Map<String, String> sharedStrings;

	private ContentTypes contentTypes;
	private String mainDocumentTarget;
	private Conformance conformance;
	private Namespace documentRelationshipsNamespace;

	// Encoding is passed in for legacy reasons, it might be
	// better to determine it ourselve
	OpenXMLZipFile(
		final ZipFile zipFile,
		final XMLInputFactory inputFactory,
		final XMLOutputFactory outputFactory,
		final XMLEventFactory eventFactory,
		final String encoding,
		final EncoderManager encoderManager,
		final IFilter subfilter,
		final Map<String, String> sharedStrings
	) {
		this.zipFile = zipFile;
		this.inputFactory = inputFactory;
		if (inputFactory.isPropertySupported(WstxInputProperties.P_MAX_ATTRIBUTE_SIZE)) {
            inputFactory.setProperty(WstxInputProperties.P_MAX_ATTRIBUTE_SIZE, MAX_ATTRIBUTE_SIZE);
        }
		this.outputFactory = outputFactory;
		this.eventFactory = eventFactory;
		this.encoding = encoding;
		this.encoderManager = encoderManager;
		this.subfilter = subfilter;
		this.sharedStrings = sharedStrings;
	}

	/**
	 * Determine the main part from the officeDocument relationship in the root
	 * rels file, then use its content type to figure out what kind of document
	 * this is.
	 * @param params parameters
	 * @return the document type 
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	DocumentType createDocument(ConditionalParameters params) throws XMLStreamException, IOException {
		initializeContentTypes();
		initializeMainDocumentTargetAndDocumentRelationshipsNamespace();

		final DocumentType doc;

		switch (contentTypes.getContentType(mainDocumentTarget)) {
			case Word.MAIN_DOCUMENT_TYPE:
			case Word.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
			case Word.TEMPLATE_DOCUMENT_TYPE:
			case Word.MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE:
				doc = new WordDocument(this, params);
				break;

			case Powerpoint.MAIN_DOCUMENT_TYPE:
			case Powerpoint.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
			case Powerpoint.SLIDE_SHOW_DOCUMENT_TYPE:
			case Powerpoint.MACRO_ENABLED_SLIDE_SHOW_DOCUMENT_TYPE:
			case Powerpoint.TEMPLATE_DOCUMENT_TYPE:
			case Powerpoint.MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE:
				doc = new PowerpointDocument(this, params);
				break;

			case Excel.MAIN_DOCUMENT_TYPE:
			case Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
			case Excel.TEMPLATE_DOCUMENT_TYPE:
			case Excel.MACRO_ENABLED_TEMPLATE_DOCUMENT_TYPE:
				doc = new ExcelDocument(this, params, encoderManager, subfilter, sharedStrings);
				break;

			case ContentTypes.Types.Visio.MAIN_DOCUMENT_TYPE:
			case ContentTypes.Types.Visio.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				doc = new VisioDocument(this, params);
				break;

			default:
				throw new OkapiBadFilterInputException(String.format("%s: %s", UNSUPPORTED_MAIN_DOCUMENT_TARGET, mainDocumentTarget));
		}
		doc.initialize();

		return doc;
	}

	private void initializeContentTypes() throws XMLStreamException, IOException {
		if (contentTypes == null) {
			contentTypes = new ContentTypes(inputFactory);
			contentTypes.parseFromXML(getPartReader(CONTENT_TYPES_PART));
		}
	}

	private void initializeMainDocumentTargetAndDocumentRelationshipsNamespace() throws IOException, XMLStreamException {
		final Relationships relationships = getRelationships(ROOT_RELS_PART);
		final String officeDocumentSourceType = Namespace.DOCUMENT_RELATIONSHIPS.concat(OFFICE_DOCUMENT);
		final String strictOfficeDocumentSourceType = Namespace.STRICT_DOCUMENT_RELATIONSHIPS.concat(OFFICE_DOCUMENT);
		final String visioDocumentSourceType = Namespace.VISIO_DOCUMENT_RELATIONSHIPS.concat(DOCUMENT);

		if (relationships.hasRelType(officeDocumentSourceType)) {
			this.mainDocumentTarget = relationships
				.getRelByType(officeDocumentSourceType)
				.get(0)
				.target;
			this.documentRelationshipsNamespace = new Namespace.Default(Namespace.DOCUMENT_RELATIONSHIPS);
		} else if (relationships.hasRelType(strictOfficeDocumentSourceType)) {
			this.mainDocumentTarget = relationships
				.getRelByType(strictOfficeDocumentSourceType)
				.get(0)
				.target;
			this.documentRelationshipsNamespace = new Namespace.Default(Namespace.STRICT_DOCUMENT_RELATIONSHIPS);
		} else if (relationships.hasRelType(visioDocumentSourceType)) {
			this.mainDocumentTarget = relationships
					.getRelByType(visioDocumentSourceType)
					.get(0)
					.target;
			this.documentRelationshipsNamespace = new Namespace.Default(Namespace.VISIO_DOCUMENT_RELATIONSHIPS);
		} else {
			throw new OkapiBadFilterInputException(UNSUPPORTED_MAIN_DOCUMENT_TARGET);
		}
	}

	ContentTypes getContentTypes() {
		return contentTypes;
	}

	XMLInputFactory getInputFactory() {
		return inputFactory;
	}

	XMLOutputFactory getOutputFactory() {
		return outputFactory;
	}

	XMLEventFactory getEventFactory() {
		return eventFactory;
	}

	String getMainDocumentTarget() {
		return mainDocumentTarget;
	}

	Namespace documentRelationshipsNamespace() {
		return this.documentRelationshipsNamespace;
	}

	/**
	 * Return a reader for the named document part. The encoding passed to
	 * the constructor will be used to decode the content.  Bad things will
	 * happen if you call this on a binary part.
	 * @param partName name of the part. Should not contain a leading '/'.
	 * @return Reader
	 * @throws IOException if any error is encountered while reading the from the zip file
	 */
	Reader getPartReader(String partName) throws IOException {
		ZipEntry entry = zipFile.getEntry(partName);
		if (entry == null) {
			throw new OkapiBadFilterInputException("File is missing " + partName);
		}
		// OpenXML documents produced by Office generally don't include BOMs, but
		// they may appear in documents produced by other sources
		return Util.skipBOM(new InputStreamReader(zipFile.getInputStream(entry), encoding));
	}

	private boolean isDocumentPartAvailable(String partName) {
		return zipFile.getEntry(partName) != null;
	}

	/**
	 * Parse the named document part as a relationships file and return the parsed
	 * relationships data.
	 * @param relsPartName name of the part. Should not contain a leading '/'.
	 * @return {@link Relationships} instance
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	Relationships getRelationships(String relsPartName) throws IOException, XMLStreamException {
		Relationships rels = new Relationships(inputFactory);
		if (isDocumentPartAvailable(relsPartName)) {
			rels.parseFromXML(relsPartName, getPartReader(relsPartName));
		}
		return rels;
	}

	/**
	 * Find the relationships file for the named part and then parse the relationships.
	 * If no relationships file exists for the specified part, an empty Relationships
	 * object is returned.
	 * @param target
	 * @return
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	Relationships getRelationshipsForTarget(String target) throws IOException, XMLStreamException {
		int lastSlash = target.lastIndexOf("/");
		if (lastSlash == -1) {
			return getRelationships("_rels/" + target + ".rels");
		}
		String relPart = target.substring(0, lastSlash) + "/_rels" + target.substring(lastSlash) + ".rels";
		return getRelationships(relPart);
	}

	InputStream getInputStream(ZipEntry entry) throws IOException {
		return zipFile.getInputStream(entry);
	}

	ZipFile getZip() {
		return zipFile;
	}

	void close() throws IOException {
		zipFile.close();
	}

	Enumeration<? extends ZipEntry> entries() {
		return zipFile.entries();
	}

	enum Conformance {
		STRICT("strict"),
		TRANSITIONAL("transitional");

		private String conformance;

		Conformance(String conformance) {
			this.conformance = conformance;
		}
	}
}
