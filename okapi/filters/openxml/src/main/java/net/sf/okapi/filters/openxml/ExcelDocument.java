/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import net.sf.okapi.common.encoder.EncoderManager;
import net.sf.okapi.common.exceptions.OkapiBadFilterInputException;
import net.sf.okapi.common.filters.IFilter;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Common;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Drawing;
import net.sf.okapi.filters.openxml.ContentTypes.Types.Excel;

import javax.xml.stream.XMLEventReader;
import javax.xml.stream.XMLStreamException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.zip.ZipEntry;

import static net.sf.okapi.filters.openxml.ParseType.MSEXCEL;
import static net.sf.okapi.filters.openxml.ParseType.MSWORDDOCPROPERTIES;

class ExcelDocument extends DocumentType {
	private static final String COMMENTS = "/comments";
	private static final String DRAWINGS = "/drawing";
	private static final String CHART = "/chart";
	private static final String DIAGRAM_DATA = "/diagramData";
	private static final String SHARED_STRINGS = "/sharedStrings";
	private static final String STYLES = "/styles";

	private final EncoderManager encoderManager;
	private final IFilter subfilter;
	private final Map<String, String> sharedStrings;

	private SharedStringMap sharedStringMap = new SharedStringMap();

	private List<String> worksheetEntryNames = null;

	private ExcelStyles styles;

	private Relationships workbookRels;

	private Map<String, ExcelWorkbook.Sheet> worksheets = new HashMap<>();

	private Map<String, Boolean> tableVisibility = new HashMap<>();

	private Map<String, String> sheetsByComment = new HashMap<>();

	private Map<String, String> sheetsByDrawing = new HashMap<>();

	private Map<String, String> drawingsByChart = new HashMap<>();

	private Map<String, String> drawingsByDiagramData = new HashMap<>();


	ExcelDocument(OpenXMLZipFile zipFile, ConditionalParameters params, EncoderManager encoderManager, IFilter subfilter, Map<String, String> sharedStrings) {
		super(zipFile, params);
		this.encoderManager = encoderManager;
		this.subfilter = subfilter;
		this.sharedStrings = sharedStrings;
	}

	@Override
	boolean isClarifiablePart(String contentType) {
		return Excel.STYLES_TYPE.equals(contentType)
				|| Excel.WORKSHEET_TYPE.equals(contentType);
	}

	@Override
	boolean isStyledTextPart(String entryName, String type) {
	    switch (type) {
            case Excel.SHARED_STRINGS_TYPE:
            case Drawing.CHART_TYPE:
            case Drawing.DIAGRAM_TYPE:
            case Excel.DRAWINGS_TYPE:
            case Excel.COMMENT_TYPE:
			    return true;
            default:
                return false;
		}
	}

	@Override
	void initialize() throws IOException, XMLStreamException {
		String mainDocumentPart = getZipFile().getMainDocumentTarget();
		workbookRels = getZipFile().getRelationshipsForTarget(mainDocumentPart);
		worksheetEntryNames = findWorksheets();
		styles = parseStyles();

		sheetsByComment = findComments(worksheetEntryNames);
		sheetsByDrawing = findDrawings(worksheetEntryNames);
		drawingsByChart = findCharts(sheetsByDrawing.keySet());
		drawingsByDiagramData = findDiagramData(sheetsByDrawing.keySet());
	}

	@Override
	OpenXMLPartHandler getHandlerForFile(ZipEntry entry, String contentType) {
		if (isPartHidden(entry.getName(), contentType)) {
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}

		// find content handler based on content type
		if (!isTranslatablePart(entry.getName(), contentType)) {
			if (contentType.equals(Excel.WORKSHEET_TYPE)) {
				// Check to see if it's visible
				return new ExcelWorksheetPartHandler(getZipFile(), entry, sharedStringMap, styles, tableVisibility,
						findWorksheetNumber(entry.getName()), getParams(), isSheetHidden(entry.getName()),
						worksheets.get(entry.getName()).getName());
			}
			else if (isClarifiablePart(contentType)) {
				return new ClarifiablePartHandler(getZipFile(), entry);
			}
			return new NonTranslatablePartHandler(getZipFile(), entry);
		}

		final StyleDefinitions styleDefinitions = new EmptyStyleDefinitions();
		final StyleOptimisation styleOptimisation = new StyleOptimisation.Bypass();

		switch (contentType) {
			case Excel.SHARED_STRINGS_TYPE:
				return new SharedStringsPartHandler(getParams(), getZipFile(), entry, styleDefinitions,
						styleOptimisation, encoderManager, subfilter, sharedStringMap);
			case Excel.COMMENT_TYPE:
				return new ExcelCommentPartHandler(getParams(), getZipFile(), entry, styleDefinitions, styleOptimisation);
			case Excel.DRAWINGS_TYPE:
			case Drawing.CHART_TYPE:
			case Drawing.DIAGRAM_TYPE:
				return new StyledTextPartHandler(
					getParams(),
					getZipFile(),
					entry,
					styleDefinitions,
					styleOptimisation
				);
			default:
				break;
		}

		// find content handler based on parseType
		ParseType parseType = null;
		switch (contentType) {
			case Common.CORE_PROPERTIES_TYPE:
				parseType = MSWORDDOCPROPERTIES;
				break;
			case Excel.MAIN_DOCUMENT_TYPE:
				parseType = MSEXCEL;
				break;
		}

		if (MSWORDDOCPROPERTIES.equals(parseType) || MSEXCEL.equals(parseType)) {
			OpenXMLContentFilter openXMLContentFilter = new OpenXMLContentFilter(getParams(), entry.getName());
			openXMLContentFilter.setUpConfig(parseType);
			return new StandardPartHandler(openXMLContentFilter, getParams(), getZipFile(), entry);
		}

		return new ExcelFormulaPartHandler(getParams(), getZipFile(), entry, sharedStrings);
	}

	private boolean isSheetHidden(String entryName) {
		ExcelWorkbook.Sheet sheet = worksheets.get(entryName);

		return sheet != null && !sheet.visible;
	}

	private boolean isTranslatablePart(String entryName, String contentType) {
		if (Excel.TABLE_TYPE.equals(contentType)) {
			Boolean b = tableVisibility.get(entryName);
			// There should always be a value, but default to hiding tables we don't know about
			return (b != null) ? b : false;
		}
		if (!entryName.endsWith(".xml")) {
			return false;
		}
		switch (contentType) {
			case Excel.SHARED_STRINGS_TYPE:
			case Drawing.CHART_TYPE:
				return true;
			case Excel.MAIN_DOCUMENT_TYPE:
			case Excel.MACRO_ENABLED_MAIN_DOCUMENT_TYPE:
				return getParams().getTranslateExcelSheetNames();
			case Common.CORE_PROPERTIES_TYPE:
				return getParams().getTranslateDocProperties();
			case Excel.COMMENT_TYPE:
				return getParams().getTranslateComments();
			case Excel.DRAWINGS_TYPE:
				return getParams().getTranslateExcelDrawings();
			case Drawing.DIAGRAM_TYPE:
				return getParams().getTranslateExcelDiagramData();
			default:
				return false;
		}
	}

	/**
	 * Do additional reordering of the entries for XLSX files to make
	 * sure that worksheets are parsed in order, followed by the shared
	 * strings table.
	 * @return the sorted enum of ZipEntry
	 * @throws IOException if any error is encountered while reading the stream
	 * @throws XMLStreamException if any error is encountered while parsing the XML
	 */
	@Override
	Enumeration<? extends ZipEntry> getZipFileEntries() throws IOException, XMLStreamException {
		Enumeration<? extends ZipEntry> entries = getZipFile().entries();
		List<? extends ZipEntry> entryList = Collections.list(entries);
		List<String> worksheetsAndSharedStrings = new ArrayList<>(worksheetEntryNames);
		worksheetsAndSharedStrings.addAll(findSharedStrings());
		entryList.sort(new ZipEntryComparator(worksheetsAndSharedStrings));
		return Collections.enumeration(entryList);
	}

	private ExcelWorkbook parseWorkbook(String partName) throws IOException, XMLStreamException {
		XMLEventReader r = getZipFile().getInputFactory().createXMLEventReader(getZipFile().getPartReader(partName));
		return new ExcelWorkbook().parseFrom(r, getParams());
	}

	private ExcelStyles parseStyles() throws IOException, XMLStreamException {
		final String namespaceUri = getZipFile().documentRelationshipsNamespace().uri();
		Relationships.Rel stylesRel = workbookRels.getRelByType(namespaceUri.concat(STYLES)).get(0);
		ExcelStyles styles = new ExcelStyles();
		styles.parse(getZipFile().getInputFactory().createXMLEventReader(
					 getZipFile().getPartReader(stylesRel.target)));
		return styles;
	}

	/**
	 * Examine relationship information to find all worksheets in the package.
	 * Return a list of their entry names, in order.
	 * @return list of entry names.
	 * @throws XMLStreamException
	 * @throws IOException
	 */
	List<String> findWorksheets() throws IOException, XMLStreamException {
		List<String> worksheetNames = new ArrayList<>();
		ExcelWorkbook workbook = parseWorkbook(getZipFile().getMainDocumentTarget());

		List<ExcelWorkbook.Sheet> sheets = workbook.getSheets();
		for (ExcelWorkbook.Sheet sheet : sheets) {
			Relationships.Rel sheetRel = workbookRels.getRelById(sheet.relId);
			worksheetNames.add(sheetRel.target);
			worksheets.put(sheetRel.target, sheet);
		}
		return worksheetNames;
	}

	/**
	 * Parse relationship information to find the shared strings table.
	 * @return an empty list if there is no the shared strings relationship
	 *         or a singleton list of found shared strings entry name otherwise.
	 * @throws IOException
	 * @throws XMLStreamException
	 */
	List<String> findSharedStrings() throws IOException, XMLStreamException {
		String mainDocumentPart = getZipFile().getMainDocumentTarget();
		Relationships rels = getZipFile().getRelationshipsForTarget(mainDocumentPart);
		final String sharedStringsNamespaceUri = getZipFile().documentRelationshipsNamespace()
				.uri().concat(SHARED_STRINGS);
		List<Relationships.Rel> r = rels.getRelByType(sharedStringsNamespaceUri);
		if (r == null) {
			return Collections.emptyList();
		}
		if (r.size() != 1) {
			throw new OkapiBadFilterInputException(
				String.format("%s: %s", UNEXPECTED_NUMBER_OF_RELATIONSHIPS, sharedStringsNamespaceUri)
			);
		}
		return Collections.singletonList(r.get(0).target);
	}

	private int findWorksheetNumber(String worksheetEntryName) {
		for (int i = 0; i < worksheetEntryNames.size(); i++) {
			if (worksheetEntryName.equals(worksheetEntryNames.get(i))) {
				return i + 1; // 1-indexed
			}
		}
		throw new IllegalStateException("No worksheet entry with name " +
						worksheetEntryName + " in " + worksheetEntryNames);
	}

	private boolean isPartHidden(String entryName, String contentType) {
	    switch (contentType) {
	        case Excel.COMMENT_TYPE:
                return isCommentHidden(entryName);
	        case Excel.DRAWINGS_TYPE:
                return isDrawingHidden(entryName);
	        case Drawing.CHART_TYPE:
                return isChartHidden(entryName);
	        case Drawing.DIAGRAM_TYPE:
                return isDiagramDataHidden(entryName);
            default:
                return false;
        }
	}

	private boolean isCommentHidden(String entryName) {
		if (!sheetsByComment.containsKey(entryName)) {
			return false;
		}

		String sheetEntryName = sheetsByComment.get(entryName);
		return isSheetHidden(sheetEntryName);
	}

	private boolean isDrawingHidden(String entryName) {
		if (!sheetsByDrawing.containsKey(entryName)) {
			return false;
		}

		String sheetEntryName = sheetsByDrawing.get(entryName);
		return isSheetHidden(sheetEntryName);
	}

	private boolean isChartHidden(String entryName) {
		if (!drawingsByChart.containsKey(entryName)) {
			return false;
		}

		String drawingEntryName = drawingsByChart.get(entryName);
		return isDrawingHidden(drawingEntryName);
	}

	private boolean isDiagramDataHidden(String entryName) {
		if (!drawingsByDiagramData.containsKey(entryName)) {
			return false;
		}

		String drawingEntryName = drawingsByDiagramData.get(entryName);
		return isDrawingHidden(drawingEntryName);
	}

	private Map<String, String> findComments(List<String> sheetEntryNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = getZipFile().documentRelationshipsNamespace().uri();
		return initializeRelsByEntry(sheetEntryNames, namespaceUri.concat(COMMENTS));
	}

	private Map<String, String> findDrawings(List<String> sheetEntryNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = getZipFile().documentRelationshipsNamespace().uri();
		return initializeRelsByEntry(sheetEntryNames, namespaceUri.concat(DRAWINGS));
	}

	private Map<String, String> findCharts(Set<String> drawingEntryNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = getZipFile().documentRelationshipsNamespace().uri();
		return initializeRelsByEntry(new ArrayList<>(drawingEntryNames), namespaceUri.concat(CHART));
	}

	private Map<String, String> findDiagramData(Set<String> drawingEntryNames)
			throws IOException, XMLStreamException {
		final String namespaceUri = getZipFile().documentRelationshipsNamespace().uri();
		return initializeRelsByEntry(new ArrayList<>(drawingEntryNames), namespaceUri.concat(DIAGRAM_DATA));
	}
}
