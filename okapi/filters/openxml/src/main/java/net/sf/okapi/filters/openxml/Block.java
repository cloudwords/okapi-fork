/*===========================================================================
  Copyright (C) 2016-2017 by the Okapi Framework contributors
-----------------------------------------------------------------------------
  Licensed under the Apache License, Version 2.0 (the "License");
  you may not use this file except in compliance with the License.
  You may obtain a copy of the License at

  http://www.apache.org/licenses/LICENSE-2.0

  Unless required by applicable law or agreed to in writing, software
  distributed under the License is distributed on an "AS IS" BASIS,
  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
  See the License for the specific language governing permissions and
  limitations under the License.
===========================================================================*/

package net.sf.okapi.filters.openxml;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedList;
import java.util.List;
import java.util.ListIterator;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.events.XMLEvent;

/**
 * A block consists of a sequence of content chunks, each
 * of which is either BlockMarkup or a Run.
 */
class Block implements XMLEvents, Textual {
	private final List<Chunk> chunks;
	private final QName runName;
	private final QName textName;
	private final boolean hidden;
	private final boolean skipped;
	private final Collection<XMLEvent> deferredEvents;

	Block(
		final List<Chunk> chunks,
		final QName runName,
		final QName textName,
		final boolean hidden,
		final boolean skipped,
		final Collection<XMLEvent> deferredEvents
	) {
		this.chunks = chunks;
		this.runName = runName;
		this.textName = textName;
		this.hidden = hidden;
		this.skipped = skipped;
		this.deferredEvents = deferredEvents;
	}

	/**
	 * Return the QName of the element that contains run data in this block.
	 */
	QName getRunName() {
		return runName;
	}

	/**
	 * Return the QName of the element that contains text data in this block.
	 */
	QName getTextName() {
		return textName;
	}

	@Override
	public List<XMLEvent> getEvents() {
		List<XMLEvent> events = new ArrayList<>();
		for (XMLEvents chunk : chunks) {
			events.addAll(chunk.getEvents());
		}
		return events;
	}

	public List<Chunk> getChunks() {
		return chunks;
	}

	boolean isHidden() {
		return hidden;
	}

	boolean isSkipped() {
		return skipped;
	}

	boolean hasVisibleRunContent() {
		for (Chunk chunk : chunks) {
			if (chunk instanceof Run) {
				if (((Run)chunk).containsVisibleText()) {
					return true;
				}
			}
			else if (chunk instanceof RunContainer) {
				if (((RunContainer)chunk).containsVisibleText()) {
					return true;
				}
			}
		}
		return false;
	}

	Collection<XMLEvent> deferredEvents() {
		return deferredEvents;
	}

	@Override
	public String toString() {
		return "Block [" + chunks + "]";
	}

	/**
	 * Marker interface to distinguish XMLEvents implementation that
	 * can be added to a Block.
	 */
	public interface BlockChunk extends Chunk { }

	static class BlockMarkup extends Markup implements BlockChunk {

		private static final String START_MARKUP_COMPONENT_IS_NOT_FOUND =
			"Unexpected structure: the start markup component is not found";

		BlockProperties blockProperties() {
			for (MarkupComponent markupComponent : getComponents()) {
				if (markupComponent instanceof BlockProperties) {
					return (BlockProperties) markupComponent;
				}
			}

			return null;
		}

		void updateOrAddComponent(final BlockProperties blockProperties) {
			final ListIterator<MarkupComponent> componentsIterator = getComponents().listIterator();
			while (componentsIterator.hasNext()) {
				final MarkupComponent markupComponent = componentsIterator.next();
				if (markupComponent instanceof BlockProperties) {
					componentsIterator.set(blockProperties);
					return;
				}
			}
			rewindAfterStartMarkupComponent(componentsIterator);
			componentsIterator.add(blockProperties);
		}

		private static void rewindAfterStartMarkupComponent(final ListIterator<MarkupComponent> iterator) {
			while (iterator.hasPrevious()) {
				if (iterator.previous() instanceof MarkupComponent.StartMarkupComponent) {
					iterator.next();
					return;
				}
			}
			throw new IllegalStateException(START_MARKUP_COMPONENT_IS_NOT_FOUND);
		}
	}

	static class Builder implements ChunkContainer {
		private List<Chunk> chunks = new ArrayList<>();
		private List<XMLEvent> currentMarkupComponentEvents = new ArrayList<>();
		private Collection<XMLEvent> deferredEvents = new LinkedList<>();
		private Markup markup = new Block.BlockMarkup();
		private boolean hidden = false;
		private boolean skipped = false;
		private QName runName;
		private QName textName;

		boolean isHidden() {
			return hidden;
		}

		void setHidden(boolean hidden) {
			this.hidden = hidden;
		}

		void setSkipped(boolean skipped) {
			this.skipped = skipped;
		}

		void setRunName(QName runName) {
			if (this.runName == null) {
				this.runName = runName;
			}
		}

		void setTextName(QName textName) {
			if (this.textName == null) {
				this.textName = textName;
			}
		}

		void flushMarkup() {
			if (!currentMarkupComponentEvents.isEmpty()) {
				markup.addComponent(MarkupComponentFactory.createGeneralMarkupComponent(currentMarkupComponentEvents));
				currentMarkupComponentEvents = new ArrayList<>();
			}
			if (!markup.getComponents().isEmpty()) {
				chunks.add(markup);
				markup = new Block.BlockMarkup();
			}
		}

		void addEvent(XMLEvent event) {
			currentMarkupComponentEvents.add(event);
		}

		@Override
		public void addChunk(Block.BlockChunk chunk) {
			flushMarkup();
			chunks.add(chunk);
		}

		void addMarkupComponent(MarkupComponent markupComponent) {
			if (!currentMarkupComponentEvents.isEmpty()) {
				markup.addComponent(MarkupComponentFactory.createGeneralMarkupComponent(currentMarkupComponentEvents));
				currentMarkupComponentEvents = new ArrayList<>();
			}
			markup.addComponent(markupComponent);
		}

		void addDeferredEvents(final Collection<XMLEvent> deferredEvents) {
			this.deferredEvents.addAll(deferredEvents);
		}

		Block buildWith(final StyleOptimisation styleOptimisation) throws XMLStreamException {
			flushMarkup();
			return new Block(styleOptimisation.applyTo(chunks), runName, textName, hidden, skipped, deferredEvents);
		}
	}
}
